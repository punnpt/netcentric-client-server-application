import socket
import select
from connection import Connection


class Server(Connection):
    def __init__(self, server_address=None):
        super().__init__()
        self.is_server = True
        if not server_address:
            server_address = socket.gethostbyname(socket.gethostname()), 9999
        self.server_address = server_address

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # AF_INET = IPv4, SOCK_STREAM = TCP
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # 1 is for True
        self.server_socket.bind(self.server_address)

        self.connections = []  # client sockets
        self.addresses = {}  # client address

        self.error_logs = []

    def start(self):
        self.server_socket.listen(5)
        print('Server is listening for connection ...')

    # def stop(self):
    #     self.server_socket.listen(0)
    #     print('Server stops listening.')
    # # clear data?

    def get_connection(self):
        if not self.server_socket:
            return
        read_sockets, _, exception_sockets = select.select([self.server_socket], [], [self.server_socket], 0)
        if not read_sockets:
            return
        connection, address = self.server_socket.accept()

        print(f'Received connection from {address} ...')
        self.connections.append(connection)
        self.addresses[connection] = address

    def send_message(self, connection, message):
        try:
            self.send(connection, message)
            return True
        except socket.error as e:
            return e
        except Exception as e:
            self.crashed('Something went wrong while sending a message', e)
            return

    def receive_messages(self):
        if not self.connections:
            return
        read_sockets, _, exception_sockets = select.select(self.connections, [], self.connections, 0)
        for connection in read_sockets:
            try:
                for message in self.receive(connection):
                    yield connection, message
            except socket.error as e:
                yield connection, e
            except Exception as e:
                self.crashed('Something went wrong while receiving messages', e)

        for connection in exception_sockets:
            yield connection, ''

    def close(self, connection):
        if connection in self.connections:
            self.connections.remove(connection)
        if connection in self.addresses:
            del self.addresses[connection]

        # if user.sitting:
        #     no, side = user.sitting
        #     self.rooms[no].clear(side)
        #     self.broadcast_message(Message(MType.SERVER_ACCEPT_LEAVE, (no, side, user.name)))
        connection.close()

