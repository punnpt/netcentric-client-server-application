from server import Server
from game_screen import *
from message import *
from map import *
import os

DEBUG = False
HOST = '172.20.10.3'
PORT = 9999
ADDRESS = [(HOST, PORT), None][DEBUG]


class EscapePlanServer:
    def __init__(self):
        pg.init()
        pg.key.set_repeat(100, 30)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)  # set screen position
        self.WIDTH = 1000
        self.HEIGHT = 700
        self.SIZE = self.WIDTH, self.HEIGHT
        self.FRAME_PER_SEC = 25

        self.screen = pg.display.set_mode((self.WIDTH, self.HEIGHT))
        pg.display.set_caption("Escape Plan Server")
        image_path = os.getcwd() + os.sep + "pictures" + os.sep
        icon = pg.image.load(image_path + "prisoner.png")
        pg.display.set_icon(icon)
        self.clock = pg.time.Clock()

        self.users = {}  # user_name -> user
        self.user_names = {}  # connection -> user_name

        self.socket = Server(ADDRESS)

        self.message_backlogs = []
        self.rooms = []

    def get_user(self, connection):
        return self.users.get(self.user_names.get(connection, None), None)

    def run(self):
        while True:
            StartUpScreen(self).run()
            MonitorScreen(self).run()

    def send_message(self, connection, message):
        result = self.socket.send_message(connection, message)
        if result is True:
            return
        if isinstance(result, Exception):
            self.socket.crashed(f'Could not send a message to {self.user_names[connection]}', result)
            self.close_connection(connection)
            return

    def broadcast_message(self, message):
        for user in self.users.values():
            self.send_message(user.connection, message)

    def close_connection(self, connection):
        self.socket.close(connection)
        if connection in self.user_names:
            user_name = self.user_names[connection]
            del self.user_names[connection]
            if user_name in self.users:
                user = self.users[user_name]
                del self.users[user_name]
                if user.sitting:
                    print('Clear user')
                    no, side = user.sitting
                    self.rooms[no].clear(side)
                    self.broadcast_message(Message(MType.SERVER_ACCEPT_LEAVE, (no, side, user.name)))
                    another_side = side ^ 1
                    if self.rooms[no].states[another_side] != RoomInfo.AVAILABLE:
                        another_name = self.rooms[no].user_names[another_side]
                        self.rooms[no].clear(another_side)
                        self.broadcast_message(Message(MType.SERVER_ACCEPT_LEAVE, (no, another_side, another_name)))

            print(f'User {user_name} disconnected')
            self.broadcast_message(Message(MType.USERS, list(self.users)))
            self.broadcast_message(Message(MType.CHAT, Chat('SERVER', f'Goodbye <{user_name}>!')))

    def update(self, rect=None):
        self.clock.tick(self.FRAME_PER_SEC)
        if not rect:
            pg.display.update()
        pg.display.update(rect)


class StartUpScreen(Screen):
    def __init__(self, app):
        super().__init__(app)
        self.back_color = (30, 30, 30)

        def start_click(sender, event):
            self.socket.start()
            self.done = True

        self.start_button = Button('START', size=(300, 60))
        self.start_button.center = self.center
        self.start_button.on_click = start_click
        self.add(self.start_button)

    def handle_message(self, connection, message):
        super().handle_message(connection, message)
        if not message or type(message) is not Message:
            if isinstance(message, Exception):
                self.socket.crashed(f'Could not get messages from the client {self.app.user_names[connection]}', message)
            self.app.close_connection(connection)
            return
        self.app.message_backlogs.append((connection, message))


class MonitorScreen(Screen):
    def __init__(self, app):
        super().__init__(app)
        self.back_color = (30, 30, 30)

        while self.app.message_backlogs:
            connection, message = self.app.message_backlogs.pop(0)
            self.handle_message(connection, message)

        self.total_users = Label(f'USERS ONLINE: 0', pos=(50, 10), font_size=30)
        self.add(self.total_users)

        self.reset_all_button = Button('RESET ALL', pos=(50, 600), size=(350, 50))
        self.reset_all_button.on_click = self.reset_all
        self.add(self.reset_all_button)

        self.room_screen = RoomScreen(app)
        self.add(self.room_screen)
        self.rooms = []
        for i in range(10):
            room_info = RoomInfo(i)
            self.rooms.append(room_info)
            room = Room(app, room_info)
            self.room_screen.rooms.append(room)
            self.room_screen.add(room)
        self.update()

    def reset_all(self, sender, event):
        for room_info in self.rooms:
            room_info.scores = [0, 0]
            if room_info.states == [RoomInfo.PLAYING, RoomInfo.PLAYING]:
                init_pack = InitPackage()
                self.app.broadcast_message(Message(MType.RESTART_GAME, (room_info, init_pack)))



    def update(self, rect=None):
        self.total_users.text = f'USERS ONLINE: {len(self.app.users)}'
        for room in self.room_screen.rooms:
            for i in range(2):
                room.user_names[i].text = room.info.user_names[i]
                room.scores_label.text = room.info.get_scores_text()
        super().update(rect)

    @property
    def rooms(self):
        return self.app.rooms

    @rooms.setter
    def rooms(self, rooms):
        self.app.rooms = rooms

    def handle_message(self, connection, message):
        super().handle_message(connection, message)
        if not message or type(message) is not Message:
            user = self.app.get_user(connection)
            self.app.close_connection(connection)
            if user.sitting:
                no, side = user.sitting
                self.rooms[no].clear(side)
                self.app.broadcast_message(Message(MType.SERVER_ACCEPT_LEAVE, (no, side, user.name)))
            return

        if message.type == MType.CLIENT_REQUEST_CONNECTION:
            self.handle_request_connection(connection, message.data.strip())
        elif message.type == MType.CLIENT_REQUEST_SIT:
            self.handle_request_sit(connection, message)
        elif message.type == MType.CLIENT_REQUEST_LEAVE:
            self.handle_request_leave(connection, message)
        elif message.type == MType.CLIENT_REQUEST_USERS:
            self.handle_request_users(connection)
        elif message.type == MType.CLIENT_REQUEST_ROOMS:
            self.handle_request_rooms(connection)
        elif message.type == MType.CHAT:
            self.handle_chat(message)
        elif message.type == MType.CLIENT_REQUEST_MOVE:
            self.handle_request_move(connection, message)
        elif message.type == MType.CLIENT_WIN:
            self.handle_client_win(connection)
        else:
            print('Invalid message type.')

    def handle_request_connection(self, connection, user_name):
        address = self.socket.addresses[connection]
        # no user coming in, user = False
        if not user_name:
            print(f'Refuse connection from {address}')
            self.app.send_message(connection, Message(MType.SERVER_REFUSE_CONNECTION, 'Username cannot be blank.'))
            self.app.close_connection(connection)
            return

        # user_name is already used
        if user_name in self.app.users:
            print(f'Refuse connection from {address}')
            self.app.send_message(connection, Message(MType.SERVER_REFUSE_CONNECTION,
                                                  f'Username "{user_name}" is already used by another user'))
            self.app.close_connection(connection)
            return

        # if user accepted, add user
        user = User(connection, address, user_name)
        self.app.user_names[connection] = user_name
        self.app.users[user_name] = user
        self.app.send_message(connection, Message(MType.SERVER_ACCEPT_CONNECTION, (user.name, user.address)))
        print(f'Accept new connection from {user.name} with IP: {user.address[0]}, PORT: {user.address[1]}')

        self.app.broadcast_message(Message(MType.USERS, list(self.app.users)))
        self.app.broadcast_message(Message(MType.CHAT, Chat('SERVER', f'Welcome <{user.name}>!')))
        self.total_users.text = f'USERS ONLINE: {len(self.app.users)}'

    def handle_request_sit(self, connection, message):
        no, side = message.data
        user = self.app.get_user(connection)
        if self.rooms[no].states[side] != RoomInfo.AVAILABLE:
            self.app.send_message(connection, Message(MType.SERVER_REFUSE_SIT, user.name))
            print(f'Refused sit from {user.name} @ room {no}, side {side}')
            return
        self.rooms[no].enter(side, user.name)
        user.sitting = no, side
        self.app.broadcast_message(Message(MType.SERVER_ACCEPT_SIT, (no, side, user.name)))
        print(f'User {user.name} sits @ room {no}, side {side}')
        self.room_screen.rooms[no].update_info()

        if self.rooms[no].states[side ^ 1] == RoomInfo.UNAVAILABLE:
            self.rooms[no].states = [RoomInfo.PLAYING] * 2
            init_pack = InitPackage()
            self.app.broadcast_message(Message(MType.INIT_PACK, (no, init_pack)))

            name1, name2 = self.rooms[no].user_names
            announcement = f'{name1} and {name2} have started a game.'
            self.app.broadcast_message(Message(MType.CHAT, Chat('SERVER', announcement)))

    def handle_request_leave(self, connection, message):
        no, side = message.data
        user = self.app.get_user(connection)
        if self.rooms[no].user_names[side] != user.name:
            return
        self.rooms[no].clear(side)
        user.sitting = None
        self.room_screen.rooms[no].update_info()
        self.app.broadcast_message(Message(MType.SERVER_ACCEPT_LEAVE, (no, side, user.name)))
        print(f'User {user.name} leaves @ room {no}, side {side}')
        another_side = side ^ 1
        if self.rooms[no].states[another_side] != RoomInfo.AVAILABLE:
            another_name = self.rooms[no].user_names[another_side]
            self.rooms[no].clear(another_side)
            self.app.broadcast_message(Message(MType.SERVER_ACCEPT_LEAVE, (no, another_side, another_name)))

    def handle_request_users(self, connection):
        self.app.send_message(connection, Message(MType.USERS, list(self.app.users)))

    def handle_request_rooms(self, connection):
        self.app.send_message(connection, Message(MType.ROOMS, self.rooms))

    def handle_chat(self, message):
        chat = message.data
        self.app.broadcast_message(Message(MType.CHAT, chat))

    def handle_request_move(self, connection, message):
        user = self.app.get_user(connection)
        room_no, side = user.sitting
        self.app.broadcast_message(Message(MType.CLIENT_MOVE, (room_no, side, message.data)))

    def handle_client_win(self, connection):
        user = self.app.get_user(connection)
        room_no, side = user.sitting
        room_info = self.rooms[room_no]
        room_info.scores[side] += 1
        self.room_screen.rooms[room_no].update_info()

        init_pack = InitPackage()
        init_pack.roles = (Map.WARDER ^ side, Map.PRISONER ^ side)

        self.app.broadcast_message(Message(MType.RESTART_GAME, (room_info, init_pack)))

        status = ['beats', 'is beaten by'][side]
        name1, name2 = room_info.user_names
        score1, score2 = room_info.scores
        announcement = f'{name1} {status} {name2}. ({score1} - {score2})'
        self.app.broadcast_message(Message(MType.CHAT, Chat('SERVER', announcement)))


class RoomScreen(Screen):
    def __init__(self, app):
        super().__init__(app)
        self.pos = 25, 75
        self.size = 500, 500

        self.sitting = None
        self.rooms = []


class Room(Screen):
    def __init__(self, app, info):
        super().__init__(app)
        self.info = info
        self.pos = 25, 50 * info.no
        self.size = 450, 50

        left_user_name = Label('', size=(125, 40), font_size=20)
        left_user_name.font = pg.font.match_font('lucidaconsole')
        left_user_name.text_align = HAlign.RIGHT, VAlign.CENTER
        left_user_name.border = 1
        left_user_name.border_color = (0, 0, 0)
        self.add(left_user_name)

        right_user_name = Label('', size=(125, 40), font_size=20)
        right_user_name.x = 225
        right_user_name.font = pg.font.match_font('lucidaconsole')
        right_user_name.text_align = HAlign.LEFT, VAlign.CENTER
        right_user_name.border = 1
        right_user_name.border_color = (0, 0, 0)
        self.add(right_user_name)

        self.user_names = [left_user_name, right_user_name]

        self.scores_label = Label(self.info.get_scores_text(), pos=(125, 0), size=(100, 40), font_size=16, font_color=(255, 255, 200))
        self.scores_label.font = pg.font.match_font('lucidaconsole')
        self.add(self.scores_label)

    def update_info(self):
        for i in range(2):
            self.user_names[i].text = self.info.user_names[i]
            self.scores_label.text = self.info.get_scores_text()

    def update(self, rect=None):
        print('..')
        super().update(rect)

if __name__ == '__main__':
    EscapePlanServer().run()
