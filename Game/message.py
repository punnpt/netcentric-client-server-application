from enum import Enum, auto


class Message:
    def __init__(self, m_type, m_data=None):
        self.type = m_type
        self.data = m_data

    def __str__(self):
        return f'[{self.type}] {self.data}'

    def __repr__(self):
        return str(self)


class MType(Enum):

    # +++++++++++++++++++++++++++++++++ CONNECTION
    #  [Server]  <<<<<<       Client
    CLIENT_REQUEST_CONNECTION = auto()
    #   Server      >>>>>>   [Client]
    SERVER_ACCEPT_CONNECTION = auto()
    SERVER_REFUSE_CONNECTION = auto()

    # +++++++++++++++++++++++++++++++++ USERS
    #  [Server]     <<<       Client
    CLIENT_REQUEST_USERS = auto()
    #   Server      >>>>>>   [Clients]
    USERS = auto()

    # +++++++++++++++++++++++++++++++++ ROOM
    #  [Server]     <<<       Client
    CLIENT_REQUEST_SIT = auto()
    #   Server      >>>>>>   [Clients]
    SERVER_ACCEPT_SIT = auto()

    #  [Server]     <<<       Client
    CLIENT_REQUEST_LEAVE = auto()
    #   Server      >>>>>>   [Clients]
    SERVER_ACCEPT_LEAVE = auto()

    #  [Server]     <<<       Client
    CLIENT_REQUEST_ROOMS = auto()
    #   Server      >>>>>>   [Clients]
    ROOMS = auto()

    # +++++++++++++++++++++++++++++++++ GAME
    #   Server      >>>>>>   [Clients]
    INIT_PACK = auto()
    #  [Server]     <<<       Client1
    CLIENT_REQUEST_MOVE = auto()
    #   Server      >>>>>>   [Client1, Client2]
    CLIENT_MOVE = auto()
    #  [Server]     <<<       Client1
    CLIENT_WIN = auto()
    #   Server      >>>>>>   [Client1, Client2]
    RESTART_GAME = auto()

    # +++++++++++++++++++++++++++++++++ CHAT
    CHAT = auto()


class Chat:
    def __init__(self, sender, data, target=None):
        self.sender = sender
        self.data = data
        self.target = target

    def __str__(self):
        return f'{self.sender}: {self.data}'

    def __repr__(self):
        return str(self)


class RoomInfo:
    LEFT = 0
    RIGHT = 1
    AVAILABLE = 'SIT'
    UNAVAILABLE = ''
    LEAVE = 'OUT'
    PLAYING = '-'

    def __init__(self, no):
        self.no = no
        self.scores = [0, 0]
        self.user_names = ['', '']
        self.states = [RoomInfo.AVAILABLE, RoomInfo.AVAILABLE]

    def get_scores_text(self):
        if RoomInfo.AVAILABLE in self.states:
            return '  _ : _  '
        l, r = [str(e) for e in self.scores]
        return (3 - len(l)) * ' ' + f'{l} : {r}' + (3 - len(r)) * ' '

    def enter(self, side, user_name):
        self.user_names[side] = user_name
        self.states[side] = RoomInfo.UNAVAILABLE
        self.scores[side] = 0

    def clear(self, side):
        self.user_names[side] = ''
        self.states[side] = RoomInfo.AVAILABLE
        self.scores[side] = 0

    def __str__(self):
        return f'\nROOM[{self.no}:\n>>>> scores:\t{self.scores}\n>>>> states:\t{self.states}\n>>>> name:\t{self.user_names}'

    def __repr__(self):
        return str(self)