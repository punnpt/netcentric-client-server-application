import pickle
from map import *


class Connection:
    SUCCESS = 0
    CONNECTION_CLOSED = 1
    ERROR = 2

    def __init__(self):
        self.BUFFER_SIZE = 2048
        self.END = b'[end]'
        self.error_logs = []
        self.is_server = True

    def send(self, connection, message):
        try:
            bytes_data = pickle.dumps(message) + self.END
            sent = connection.send(bytes_data)
        except pickle.UnpicklingError as e:
            self.crashed('Unable to dump data', e)
            return False
        else:
            return sent

    def receive(self, connection):
        bytes_data = connection.recv(self.BUFFER_SIZE)
        if not bytes_data:
            yield ''
        try:
            while self.END in bytes_data:
                index = bytes_data.find(self.END)
                message = pickle.loads(bytes_data[:index])
                yield message
                bytes_data = bytes_data[index + len(self.END):]
        except pickle.UnpicklingError as e:
            self.crashed(f'Unable to load data "{bytes_data}"', e)
            yield None

    def crashed(self, message, e):
        print(f'{message}\n>>> {type(e)}\n>>> {e}\n')
        self.error_logs.append((message, e))

    @staticmethod
    def get_error_message(error):
        if error == Connection.CONNECTION_CLOSED:
            return 'Could not send a message to a connection'
        if error == Connection.ERROR:
            return 'Something went wrong while sending a message'
