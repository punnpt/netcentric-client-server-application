from client import Client
from game_screen import *
from message import *
import os

DEBUG = False
HOST = '172.20.10.3'
PORT = 9999
ADDRESS = [(HOST, PORT), None][DEBUG]


class EscapePlanClient:
    def __init__(self):
        pg.init()
        pg.key.set_repeat(100, 30)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)  # set screen position
        self.WIDTH = 1000
        self.HEIGHT = 700
        self.SIZE = self.WIDTH, self.HEIGHT
        self.FRAME_PER_SEC = 25
        self.TURN_TIME = 10

        self.screen = pg.display.set_mode((self.WIDTH, self.HEIGHT))
        pg.display.set_caption("Escape Plan Client")
        image_path = os.getcwd() + os.sep + "pictures" + os.sep
        icon = pg.image.load(image_path + "prisoner.png")
        pg.display.set_icon(icon)
        self.clock = pg.time.Clock()
        self.thwalkSound = pg.mixer.Sound(image_path+'OhYeah.wav')

        self.socket = Client(ADDRESS)
        self.user = None
        self.user_name = None
        self.users_list = []
        self.chat_backlogs = []
        self.room_info = [RoomInfo(i) for i in range(10)]

    def run(self):
        while True:
            LogInScreen(self).run()
            LobbyScreen(self).run()

    def update(self, rect=None):
        self.clock.tick(self.FRAME_PER_SEC)
        if not rect:
            pg.display.update()
        pg.display.update(rect)

    def handle_message(self, message):
        if message.type == MType.USERS:
            self.users_list = message.data
        elif message.type == MType.CHAT:
            self.chat_backlogs.append(message.data)


class LogInScreen(Screen):
    def __init__(self, app):
        super().__init__(app)
        self.back_color = (30, 30, 30)

        self.game_label = Label('ESCAPE PLAN', font_size=100)
        self.game_label.center = self.w * 0.5, self.h * 0.3
        self.add(self.game_label)

        def return_handler(sender, event):
            self.request_connect()

        self.user_input = TextBox(text='', size=(225, 60))
        self.user_input.font_size = 38
        self.user_input.pos = self.w * 0.5 - 200, self.h * 0.5
        self.user_input.place_holder = 'USERNAME'
        self.user_input.active = True
        self.user_input.prohibited.update([pg.K_SPACE, pg.K_TAB])
        self.user_input.limit = 8
        self.user_input.on_return = return_handler
        self.add(self.user_input)

        def click_handler(sender, event):
            self.user_input.active = True
            self.request_connect()

        self.login_btn = Button('LOG IN', size=(125, 60))
        self.login_btn.pos = self.w * 0.5 + 50, self.h * 0.5
        self.login_btn.on_click = click_handler
        self.add(self.login_btn)

        self.status_label = Label('', font_size=20)
        self.status_label.pos = self.user_input.x, self.user_input.rect.bottom + 20
        self.status_label.fore_color = (255, 100, 100)
        self.add(self.status_label)
        self.update()

        ins = Label("Instruction")
        ins.center = self.w // 2, 500
        self.add(ins)
        ins = Label("Press arrow key to move your character", font_size=20)
        ins.center = self.w // 2, 540
        self.add(ins)
        ins = Label("Prisoner : Make your way to the door", font_size=20)
        ins.center = self.w // 2, 570
        self.add(ins)
        ins = Label("Warder : Catch your prisoner", font_size=20)
        ins.center = self.w // 2, 600
        self.add(ins)
        ins = Label("Each turn you can move to any adjacent blocks but obstacles", font_size=20)
        ins.center = self.w // 2, 630
        self.add(ins)
        ins = Label("You can warp across the map through portals", font_size=20)
        ins.center = self.w // 2, 660
        self.add(ins)


    def handle_message(self, connection, message):
        super().handle_message(connection, message)
        if not message or type(message) is not Message:
            self.socket.close()
            print('Disconnected from the server.')
            return

        if message.type == MType.SERVER_REFUSE_CONNECTION:
            reason = message.data if message.data else 'Unable to connect with unknown reason.'
            self.set_status_label(reason, (255, 100, 100))
            print(reason)
        elif message.type == MType.SERVER_ACCEPT_CONNECTION:
            self.done = True
            user_name, address = message.data
            self.app.user = User(self.app.socket, address, user_name)
            self.app.user_name = user_name
            self.socket.client_address = address
            print(f'Successfully connected to the server with username "{self.app.user_name}".')
        else:
            self.app.handle_message(message)

    def request_connect(self):
        if not self.user_input.text:
            self.set_status_label('Username must not be blank.', (255, 100, 100))
            return
        self.set_status_label('Connecting, please wait ...', (100, 100, 255))
        connect_result = self.socket.connect()
        if connect_result is not True:
            self.set_status_label(connect_result, (255, 100, 100))
            return
        self.socket.send_message(Message(MType.CLIENT_REQUEST_CONNECTION, self.user_input.text))

    def set_status_label(self, status, color):
        self.status_label.text = status
        self.status_label.fore_color = color
        self.update()


class LobbyScreen(Screen):
    def __init__(self, app):
        super().__init__(app)
        self.back_color = (30, 30, 30)

        self.name_label = Label(self.app.user_name, pos=(50, 20), font_size=30)
        self.add(self.name_label)

        self.chat_screen = ChatScreen(app)
        self.add(self.chat_screen)

        self.game_screen = RoomScreen(app)
        self.add(self.game_screen)
        self.update()

    def change_screen(self, screen):
        old_screen = self.children[-1]
        self.remove(old_screen)
        self.game_screen = screen
        self.add(self.game_screen)

    def handle_message(self, connection, message):
        super().handle_message(connection, message)
        if not message or type(message) is not Message:
            self.done = True
            self.socket.close()
            print('Disconnected from server.')
            return
        if message.type == MType.CHAT:
            self.chat_screen.handle_chat(message.data)
        elif message.type == MType.USERS:
            self.app.users_list = message.data
        else:
            self.game_screen.handle_message(connection, message)
        print(self.game_screen.n)


class RoomScreen(Screen):
    def __init__(self, app):
        super().__init__(app)
        self.n = 'Room'
        self.pos = 25, 75
        self.size = 500, 500

        self.sitting = None

        self.rooms = []
        for i in range(10):
            room = Room(app, self.app.room_info[i])
            self.rooms.append(room)
            self.add(room)

        self.request_rooms_info()

    @property
    def sitting(self):
        return self.app.user.sitting

    @sitting.setter
    def sitting(self, sitting):
        self.app.user.sitting = sitting

    def request_rooms_info(self):
        self.socket.send_message(Message(MType.CLIENT_REQUEST_ROOMS))

    def handle_message(self, connection, message):
        if message.type == MType.ROOMS:
            self.app.room_info[:] = info_list = message.data
            for room, info in zip(self.rooms, info_list):
                room.info = info
                room.update_info()

        elif message.type == MType.SERVER_ACCEPT_SIT:
            self.handle_sit(*message.data)
        elif message.type == MType.SERVER_ACCEPT_LEAVE:
            self.handle_leave(*message.data)
        elif message.type == MType.INIT_PACK:
            self.handle_init_pack(*message.data)
        elif message.type == MType.RESTART_GAME:
            self.handle_restart_game(*message.data)

    def handle_restart_game(self, room_info, init_pack):
        room = self.rooms[room_info.no]
        room.info.scores[:] = room_info.scores
        for room in self.rooms:
            room.update_info()

    def handle_sit(self, no, side, user_name):
        room = self.rooms[no]
        if user_name == self.app.user_name:
            room.info.states[side] = RoomInfo.LEAVE
            self.sitting = (no, side)
        else:
            room.info.states[side] = RoomInfo.UNAVAILABLE
        room.info.user_names[side] = user_name
        print(f'<{user_name}> sits at {no}, {side}')
        for room in self.rooms:
            room.update_info()

    def handle_leave(self, no, side, user_name):
        room = self.rooms[no]
        if user_name == self.app.user_name:
            self.sitting = None
        room.info.states[side] = RoomInfo.AVAILABLE
        room.info.user_names[side] = ''
        for room in self.rooms:
            room.update_info()

    def handle_init_pack(self, no, init_pack):
        room = self.rooms[no]
        room.info.scores[:] = [0, 0]
        room.info.states[:] = [RoomInfo.PLAYING, RoomInfo.PLAYING]
        if self.sitting:
            my_room, side = self.sitting
            if my_room == no:
                self.parent.change_screen(GameScreen(self.app, init_pack))


class Room(Screen):
    def __init__(self, app, info):
        super().__init__(app)
        self.info = info
        self.pos = 25, 50 * info.no
        self.size = 450, 50

        left_button = Button(RoomInfo.AVAILABLE, size=(50, 40))
        left_button.font_size = 20
        left_button.on_click = self.request_sit
        left_button.side = RoomInfo.LEFT
        self.add(left_button)

        right_button = Button(RoomInfo.AVAILABLE, size=(50, 40))
        right_button.rect.right = self.w
        right_button.font_size = 20
        right_button.on_click = self.request_sit
        right_button.side = RoomInfo.RIGHT
        self.add(right_button)

        self.buttons = left_button, right_button

        left_user_name = Label('', size=(125, 40), font_size=20)
        left_user_name.x = left_button.rect.right
        left_user_name.font = pg.font.match_font('lucidaconsole')
        left_user_name.text_align = HAlign.RIGHT, VAlign.CENTER
        left_user_name.border = 1
        left_user_name.border_color = (0, 0, 0)
        self.add(left_user_name)

        right_user_name = Label('', size=(125, 40), font_size=20)
        right_user_name.rect.right = right_button.x
        right_user_name.font = pg.font.match_font('lucidaconsole')
        right_user_name.text_align = HAlign.LEFT, VAlign.CENTER
        right_user_name.border = 1
        right_user_name.border_color = (0, 0, 0)
        self.add(right_user_name)

        self.user_names = [left_user_name, right_user_name]

        self.scores_label = Label(self.info.get_scores_text(), pos=(175, 0), size=(100, 40), font_size=16, font_color=(255, 255, 200))
        self.scores_label.font = pg.font.match_font('lucidaconsole')
        self.add(self.scores_label)

    def update_info(self):
        for i, button in enumerate(self.buttons):
            if not self.parent.sitting:
                button.text = self.info.states[i]
                if self.info.states[i] == RoomInfo.AVAILABLE:
                    button.enabled = True
                    button.color = Color.BLUE
                    button.on_click = self.request_sit
                elif self.info.states[i] in [RoomInfo.UNAVAILABLE, RoomInfo.PLAYING]:
                    button.enabled = False
            elif self.parent.sitting == (self.info.no, i):
                button.text = self.info.states[i]
                button.color = Color.LIGHT_BLUE
                button.on_click = self.request_leave
            else:
                button.enabled = False
                button.text = ''
            self.user_names[i].text = self.info.user_names[i]
            self.scores_label.text = self.info.get_scores_text()

    def request_sit(self, sender, event):
        self.socket.send_message(Message(MType.CLIENT_REQUEST_SIT, (self.info.no, sender.side)))

    def request_leave(self, sender, event):
        self.socket.send_message(Message(MType.CLIENT_REQUEST_LEAVE, (self.info.no, sender.side)))


class ChatScreen(Screen):
    def __init__(self, app):
        super().__init__(app)
        self.back_color = (45, 45, 45)
        self.size = 400, 600
        self.rect.topright = self.app.WIDTH - 25, 75
        self.rude_words = ['damn', 'fuck', 'shit', 'ass', 'bitch']

        def return_handler(sender, event):
            text = self.user_input.text
            if text[:6] == '/users':
                sender = 'USERS :'
                text = f'[{len(self.app.users_list)}] ' + ', '.join([e for e in self.app.users_list])
                self.handle_chat(Chat(sender, text))
            elif text.strip():
                for word in self.rude_words:
                    text = text.replace(word, '*' * len(word))

                self.socket.send_message(Message(MType.CHAT, Chat(self.app.user_name, text)))
            self.user_input.text = ''

        self.user_input = TextBox(size=(self.w, 40))
        self.user_input.rect.bottom = self.h
        self.user_input.padding = 5, 0
        self.user_input.font_size = 18
        self.user_input.place_holder = 'SEND MESSAGE'
        self.user_input.prohibited.update([pg.K_TAB])
        self.user_input.limit = 30
        self.user_input.on_return = return_handler
        self.add(self.user_input)

        self.limit = 50
        self.chat_logs = []
        while self.app.chat_backlogs:
            chat = self.app.chat_backlogs.pop(0)
            self.handle_chat(chat)

    def handle_chat(self, chat):
        sender = chat.sender
        text = chat.data
        log = Label(sender + ' ' * (9 - len(sender)) + text, font_size=15)
        log.x = 5
        log.font = pg.font.match_font('lucidaconsole')
        log.text_align = HAlign.LEFT, VAlign.CENTER
        if sender == self.app.user_name:
            log.fore_color = (200, 200, 255)
        elif sender == 'SERVER':
            log.fore_color = (200, 255, 200)
        elif sender == 'USERS :':
            log.fore_color = (255, 255, 200)
        self.add(log)
        self.chat_logs.append(log)
        if len(self.chat_logs) * log.h > self.h - self.user_input.h:
            old_log = self.chat_logs.pop(0)
            self.remove(old_log)
        for i, log in enumerate(self.chat_logs):
            log.y = i * log.h


class GameScreen(Screen):
    def __init__(self, app, init_pack):
        super().__init__(app)
        self.n = 'Game'
        self.back_color = (100, 100, 100)
        self.size = 500, 550
        self.pos = 25, 75

        for r in range(5):
            for c in range(5):
                cell = Square(None, None, location=(c, r))
                cell.border = 2
                cell.border_color = (0, 0, 0)
                self.add(cell)

        image_path = os.getcwd() + os.sep + "pictures" + os.sep
        prisoner_image = pg.image.load(image_path + "prisoner.png")
        warder_image = pg.image.load(image_path + "warder.png")
        obstacle_image = pg.image.load(image_path + "obstacle.png")
        portal_image = pg.image.load(image_path + "portal.png")
        tunnel_image = pg.image.load(image_path + "tunnel.png")

        self.game_elements = []
        self.game_elements.append(Square(Map.WARDER, None, image=warder_image))
        self.game_elements.append(Square(Map.PRISONER, None, image=prisoner_image))
        self.game_elements.append(Square(Map.TUNNEL, None, image=tunnel_image))
        self.game_elements.extend([Square(Map.OBSTACLE + i, None, image=obstacle_image) for i in range(5)])
        self.game_elements.extend([Square(Map.PORTAL + i, None, image=portal_image) for i in range(2)])

        for e in self.game_elements:
            self.add(e)

        self.role_status = Label('Welcome motherfucker', size=(200, 50), font_size=16)
        self.role_status.pos = 0, 500
        self.role_status.fore_color = (255, 200, 200)
        self.timer = Label('Time remaining: 10.00', size=(200, 50), font_size=16)
        self.timer.pos = 200, 500
        self.timer.font = pg.font.match_font('lucidaconsole')
        self.score_status = Label('Score: 0', size=(100, 50), font_size=16)
        self.score_status.pos = 400, 500
        self.add(self.role_status)
        self.add(self.timer)
        self.add(self.score_status)
        self.leave_button = Button('LEAVE GAME', pos=(300, -55), size=(200, 50))
        self.leave_button.on_click = self.request_leave
        self.add(self.leave_button)

        ################
        self.init_game(init_pack)

    def init_game(self, init_pack):
        room_no, side = self.sitting
        self.map = init_pack.map
        self.role = init_pack.roles[self.sitting[1]]
        self.score = self.app.room_info[room_no].scores[side]
        self.count = 0
        self.current_turn = Map.WARDER

        for e in self.game_elements:
            e.map = init_pack.map

        self.game_elements[self.role].on_key_down = self.player_move
        self.game_elements[self.role ^ 1].on_key_down = None
        self.game_elements[self.role ^ 1].border = None

        self.player = self.game_elements[self.role]
        self.player.border = 2
        self.player.border_color = [(0, 0, 0), (100, 255, 100)][self.is_my_turn()]
        self.player.back_color = [(100, 100, 100), (200, 255, 200)][self.is_my_turn()]
        self.role_status.text = f'Welcome {["WARDER", "PRISONER"][self.role]}!'
        self.timer.text = 'Time remaining: %.2f' % round(self.app.TURN_TIME - self.count/self.app.FRAME_PER_SEC, 2)
        self.score_status.text = f'Score: {self.score}'

    def request_leave(self, sender, event):
        self.socket.send_message(Message(MType.CLIENT_REQUEST_LEAVE, self.sitting))

    def player_move(self, sender, event):
        if not self.is_my_turn():
            return
        directions = [pg.K_LEFT, pg.K_RIGHT, pg.K_UP, pg.K_DOWN]
        if event.key not in directions:
            return

        index = directions.index(event.key)
        results = sender.movable(index)
        if results[0]:
            data = {Map.PORTAL+i: portal for i, portal in enumerate(results[2])}
            data[sender.index] = results[1]
            self.socket.send_message(Message(MType.CLIENT_REQUEST_MOVE, data))

    def handle_event(self, event):
        super().handle_event(event)
        if event.type == TICK:
            self.count = self.count % (self.app.FRAME_PER_SEC * self.app.TURN_TIME) + 1
            self.timer.text = ['Waiting for opponent.', 'Time remaining: %.2f' % round(self.app.TURN_TIME - self.count/self.app.FRAME_PER_SEC, 2)][self.is_my_turn()]
            if self.is_my_turn() and self.count == self.app.FRAME_PER_SEC * self.app.TURN_TIME:
                new_loc = self.map.route_to(self.role)
                dx = [-1, 1, 0, 0]
                dy = [0, 0, -1, 1]
                ls = list(zip(dx, dy))
                d_index = ls.index(minus(new_loc, self.map.objects[self.role]))
                results = self.player.movable(d_index)
                data = {Map.PORTAL+i: portal for i, portal in enumerate(results[2])}
                data[self.role] = results[1]
                self.socket.send_message(Message(MType.CLIENT_REQUEST_MOVE, data))

    def handle_message(self, connection, message):
        if message.type == MType.CLIENT_MOVE:
            room_no, side, data = message.data
            if self.sitting[0] == room_no:
                for index, location in data.items():
                    self.map.objects[index] = location
                self.app.thwalkSound.play()
                self.count = 0
                self.current_turn ^= 1
                self.player.border_color = [(0, 0, 0), (100, 255, 100)][self.is_my_turn()]
                self.player.back_color = [(100, 100, 100), (200, 255, 200)][self.is_my_turn()]
                if self.map.check_win(self.role):
                    self.socket.send_message(Message(MType.CLIENT_WIN))

        elif message.type == MType.RESTART_GAME:
            room_info, init_pack = message.data
            room_no, side = self.sitting
            if room_no == room_info.no:
                self.app.room_info[room_no].scores[:] = room_info.scores
                self.init_game(init_pack)
        elif message.type == MType.SERVER_ACCEPT_LEAVE:
            room_no, side, user_name = message.data
            if self.sitting == (room_no, side):
                self.parent.change_screen(RoomScreen(self.app))
    @property
    def sitting(self):
        return self.app.user.sitting

    @sitting.setter
    def sitting(self, sitting):
        self.app.user.sitting = sitting

    def is_my_turn(self):
        return not (self.role ^ self.current_turn)

    # def handle_message(self, connection, message):
    #     if message.type == MType.INIT_PACK:
    #         room_no, init_pack = message.data
    #         if self.sitting:
    #             my_room, side = self.sitting
    #             if my_room == room_no:
    #
    #         for room, info in zip(self.rooms, info_list):
    #             room.info = info
    #             room.update_info()


if __name__ == '__main__':
    EscapePlanClient().run()

