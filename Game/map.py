import random as rd


class Map:
    WARDER = 0
    PRISONER = 1
    TUNNEL = 2
    OBSTACLE = 3
    PORTAL = 8

    def __init__(self):
        self.rows = 5
        self.cols = 5
        self.objects = None
        self.all_cells = {(i, j) for i in range(self.rows) for j in range(self.cols)}
        self.random_map()

    @staticmethod
    def get_role(index):
        if index is None:
            return
        if index < 3:
            return index
        if index < 8:
            return 3
        return 8

    def free_cells(self, index):
        role = Map.get_role(index)
        free = self.all_cells - set(self.objects[Map.OBSTACLE: Map.OBSTACLE + 5])
        if role == Map.WARDER:
            free.discard(self.tunnel)
        return free

    def min_distance(self, source_index, target_index, source=None, target=None):
        if not source:
            source = self.objects[source_index]
        if not target:
            target = self.objects[target_index]
        free_cells = self.free_cells(source_index) & self.free_cells(target_index)

        dx = [0, 1, 0, -1]
        dy = [-1, 0, 1, 0]
        queue = [(source, 0)]
        reached = {source}
        while len(queue) > 0:
            ((x, y), d) = queue.pop(0)
            if (x, y) == target:
                return d
            for i in range(4):
                nx = x + dx[i]
                ny = y + dy[i]
                if (nx, ny) in free_cells - reached:
                    queue.append(((nx, ny), d + 1))
                    reached.add((nx, ny))
        return -1

    def random_map(self):
        while True:
            self.objects = rd.sample(self.all_cells, 10)
            wp = self.min_distance(Map.WARDER, Map.PRISONER)
            pt = self.min_distance(Map.PRISONER, Map.TUNNEL)
            pp = self.min_distance(Map.PORTAL, Map.PORTAL + 1)
            # print(wp, pt, pp)
            if 1 < wp and 1 < pt:
                if 0 < pp:
                    break

    def check_win(self, role):
        if role == Map.PRISONER and self.objects[Map.PRISONER] == self.objects[Map.TUNNEL]:
            return True
        return role == Map.WARDER and self.objects[Map.PRISONER] == self.objects[Map.WARDER]

    def get_new_portals(self):
        return rd.sample(self.all_cells - set(self.objects[:Map.PORTAL]), 2)

    @property
    def warder(self):
        return self.objects[Map.WARDER]

    @property
    def prisoner(self):
        return self.objects[Map.PRISONER]

    @property
    def tunnel(self):
        return self.objects[Map.TUNNEL]

    @property
    def obstacles(self):
        return set(self.objects[Map.OBSTACLE: Map.OBSTACLE+5])

    @property
    def portals(self):
        return set(self.objects[Map.PORTAL:Map.PORTAL+2])

    def get_neighbor(self, role):
        pos = self.objects[role]
        dx = [0, 1, 0, -1]
        dy = [-1, 0, 1, 0]
        n = []
        for i in range(4):
            nx = pos[0] + dx[i]
            ny = pos[1] + dy[i]
            if (nx, ny) not in self.free_cells(role):
                continue
            n.append((nx, ny))
        return n

    def route_to(self, source_role):
        target_role = [Map.PRISONER, Map.TUNNEL][source_role]
        neighbor = {e: -1 for e in self.get_neighbor(source_role)}
        min = [None, 100]
        for e in neighbor:
            neighbor[e] = self.min_distance(source_role, target_role, source=e)
            if neighbor[e] < min[1]:
                min[1] = neighbor[e]
                min[0] = e
        if min[0] is None:
            return rd.choice(self.get_neighbor(source_role))
        return min[0]

class InitPackage:
    def __init__(self):
        self.map = Map()
        x = rd.randint(0, 1)
        self.roles = x, x ^ 1


if __name__ == '__main__':
    m = Map()
