import pygame as pg


class Button:
    def __init__(self, pos, size, text='Button', font=None, font_size=32, color='BLUE'):
        self.pos = self.x, self.y = pos
        self.size = self.w, self.h = size
        self.text = text
        self.set_font(font, font_size, color)

        self.hover = False
        self.clicked = False

        self.text_surface = self.font.render(text, True, self.fore_color[self.hover])
        self.text_rect = self.text_surface.get_rect(center=(self.x + self.w//2, self.y + self.h//2))
        self.rect = pg.Rect(pos, size)
        self.on_click = None

    def set_font(self, font=None, font_size=32, color='BLUE'):
        self.font = pg.font.Font(font, font_size)
        if color == 'BLUE':
            self.fore_color = [pg.Color('lightskyblue2'), pg.Color('lightskyblue1')]
            self.border_color = [pg.Color('lightskyblue3'), pg.Color('dodgerblue2')]
            self.back_color = [(20, 20, 20), pg.Color('dodgerblue1'),  pg.Color('dodgerblue4')]

    def handle_event(self, event):
        if event.type == pg.MOUSEMOTION:
            self.hover = self.rect.collidepoint(event.pos)
        elif event.type == pg.MOUSEBUTTONDOWN:
            self.clicked = self.rect.collidepoint(event.pos)
        elif event.type == pg.MOUSEBUTTONUP:
            if self.clicked and self.rect.collidepoint(event.pos):
                self.click()
            self.clicked = False

    def click(self):
        if self.on_click:
            self.on_click()

    def update(self):
        self.text_surface = self.font.render(self.text, True, self.fore_color[self.hover])
        self.text_rect = self.text_surface.get_rect(center=(self.x + self.w//2, self.y + self.h//2))

    def draw(self, screen):
        self.update()
        pg.draw.rect(screen, self.back_color[self.hover + self.clicked], self.rect)
        pg.draw.rect(screen, self.border_color[self.hover], self.rect, 2)
        screen.blit(self.text_surface, self.text_rect)


class InputBox:
    def __init__(self, x, y, w, h, text=''):
        self.font = pg.font.Font(None, 32)
        self.fore_color = [pg.Color('lightskyblue3'), pg.Color('dodgerblue2')]
        self.back_color = [(0, 0, 0, 0), (100, 100, 200)]

        self.width = w
        self.rect = pg.Rect(x, y, w, h)
        self.active = False
        self.text = text

        self.cursor = ['', '_']
        self.on_return_action = None

        self.text_surface = self.font.render(text + self.cursor[self.active], True, self.fore_color[self.active])

    def on_return(self):
        if self.on_return_action:
            self.on_return_action(self.text)
        print(self.text)
        self.text = ''

    def handle_event(self, event):
        if event.type == pg.MOUSEBUTTONDOWN:
            self.active = self.rect.collidepoint(event.pos)
        elif event.type == pg.KEYDOWN and self.active:
            if event.key == pg.K_RETURN:
                self.on_return()
            elif event.key == pg.K_BACKSPACE:
                self.text = self.text[:-1]
            else:
                self.text += event.unicode

    def update(self):
        # Resize the box if the text is too long.
        self.text_surface = self.font.render(self.text + self.cursor[self.active], True, self.fore_color[self.active])
        width = max(self.width, self.text_surface.get_width() + 10)
        self.rect.w = width

    def draw(self, screen):
        self.update()
        pg.draw.rect(screen, self.fore_color[self.active], self.rect, 2)
        screen.blit(self.text_surface, (self.rect.x + 5, self.rect.y + 5))


class Label:
    def __init__(self, pos, text='Label'):
        self.font = pg.font.Font(None, 32)
        self.color = pg.Color('lightskyblue3')
        self.pos = pos
        self.text = text
        self.txt_surface = self.font.render(text, True, self.color)

    def set_font(self, font):
        self.font = font

    def set_font_color(self, color):
        self.color = color

    def set_text(self, text):
        self.text = text

    def update(self):
        self.txt_surface = self.font.render(self.text, True, self.color)

    def handle_event(self, event):
        pass

    def draw(self, screen):
        self.update()
        screen.blit(self.txt_surface, self.pos)


class Grid:
    def __init__(self, pos, row, col, cell_width, back_color=(100, 100, 100), border_color=(0, 0, 0)):
        self.pos = pos
        self.row = row
        self.col = col
        self.cell_width = cell_width
        self.grid_width = cell_width * col
        self.grid_height = cell_width * row
        self.back_color = back_color
        self.border_color = border_color

        self.rect = pg.Rect(pos, (self.grid_width, self.grid_height))
        cell_size = cell_width, cell_width
        self.cells = [pg.Rect(self.cell_pos(r, c), cell_size) for r in range(self.row) for c in range(self.col)]

    def cell_pos(self, r, c):
        x, y = self.pos[0] + c * self.cell_width, self.pos[1] + r * self.cell_width
        return x, y

    def handle_event(self, event):
        pass

    def update(self):
        pass

    def draw(self, surface):
        pg.draw.rect(surface, self.back_color, self.rect)
        for cell in self.cells:
            pg.draw.rect(surface, self.border_color, cell, 1)


class Square:
    def __init__(self, pos, width, game_map, color=(255, 0, 0), image=None):
        self.width = width
        self.game_map = game_map
        self.pos = pos
        self.color = color
        self.image = image
        self.rect = pg.Rect(self.pos, (self.width, self.width))

    def handle_event(self, event):
        pass

    def update(self):
        self.rect = pg.Rect(self.pos, (self.width, self.width))

    def draw(self, surface):
        # x, y = self.pos
        if self.image:
            surface.blit(self.image, self.rect)
        # else:
        #     pg.draw.rect(surface, self.color, (x*self.width+1, y*self.width + 1, self.width - 1, self.width - 1))


class ChatBox:
    def __init__(self, pos, width, height):
        self.pos = pos
        self.width = width
        self.height = height

        self.chat_log = []
        self.recent_chat = []
        input_box_height = 40
        self.input_box = InputBox(pos[0], pos[1] + height - input_box_height, width, input_box_height)

        self.chat_height = height - (20 + input_box_height)
        each_chat_height = 40
        self.chat_shown = self.chat_height // each_chat_height
        self.rect = pg.Rect(pos, (width, self.chat_height))

    def handle_event(self, event):
        self.input_box.handle_event(event)

    def update(self):
        self.recent_chat = []
        for i in range(min(len(self.chat_log), self.chat_shown)):
            index = -(i+1)
            x, y = self.pos[0] + 10, self.pos[1] + self.chat_height + index * 40
            label = Label((x, y), str(self.chat_log[index]))
            self.recent_chat.append(label)

    def draw(self, screen):
        self.update()
        pg.draw.rect(screen, (50, 50, 50), self.rect)
        for label in self.recent_chat:
            label.draw(screen)
        self.input_box.draw(screen)


class UserList:
    def __init__(self, pos, size, on_click):
        self.pos = self.x, self.y = pos
        self.size = self.w, self.h = size
        self.on_click = on_click

        self.current_user = None
        self.users_list = []
        self.invite_button_list = []
        self.users_label_list = []
        self.click_user = []

        self.changed = False

        self.rect = pg.Rect(pos, size)

    def set_users_list(self, users_list, current_user):
        self.users_list = users_list
        self.current_user = current_user
        self.changed = True

    def handle_event(self, event):
        for btn in self.invite_button_list:
            btn.handle_event(event)

    def update(self):
        if not self.changed:
            return
        self.invite_button_list = []
        self.users_label_list = []
        for i, user in enumerate(self.users_list):
            w, h = 100, 40  # button
            if user != self.current_user:
                x, y = self.x + 10, self.y + 10 + i * 50
                button = Button((x, y), (w, h), 'INVITE')
                button.set_font(font_size=24)
                button.on_click = lambda: self.on_click(user)
                self.invite_button_list.append(button)
            x, y = self.x + w + 20, self.y + 20 + i * 50
            self.users_label_list.append(Label((x, y), user))
        self.changed = False

    def draw(self, screen):
        self.update()
        pg.draw.rect(screen, (45, 50, 45), self.rect)
        pg.draw.rect(screen, (65, 100, 85), self.rect, 2)
        for label in self.users_label_list:
            label.draw(screen)
        for btn in self.invite_button_list:
            btn.draw(screen)





