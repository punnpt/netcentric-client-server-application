from game_object import *
from map import Map

TICK = pg.USEREVENT + 1

class Label(GameObject):
    def __init__(self, text, pos=(0, 0), size=(0, 0), font_size=32, font_color=pg.Color('white')):
        self.fit_size = (size == (0, 0))
        super().__init__(pos, size)
        self.font = pg.font.match_font('lucidasans')
        self.font_size = font_size
        self.fore_color = font_color
        self.text = text
        self.text_align = HAlign.CENTER, VAlign.CENTER

    @property
    def fit_size(self):
        return self._fit_size

    @fit_size.setter
    def fit_size(self, fit_size):
        self._fit_size = fit_size

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        self._text = text
        if self.fit_size:
            self.size = self.text_surface.get_size()

    @property
    def font(self):
        return self._font

    @font.setter
    def font(self, font):
        self._font = font

    @property
    def font_size(self):
        return self._font_size

    @font_size.setter
    def font_size(self, font_size):
        self._font_size = font_size

    @property
    def text_surface(self):
        if not self.fore_color:
            return None
        return get_text_surface(self.text, self.font, self.font_size, self.fore_color)

    @property
    def text_align(self):
        return self._text_align

    @text_align.setter
    def text_align(self, text_align):
        self._text_align = text_align

    @property
    def text_rect(self):
        align = mul(self.text_align, 0.5)
        text_rect = self.text_surface.get_rect()
        text_rect.center = self.true_rect.center
        direction = dot(align, minus(self.size, text_rect.size))
        text_rect.move_ip(*direction)
        return text_rect

    def draw(self, screen):
        super().draw(screen)
        screen.blit(self.text_surface, self.text_rect)


class Button(Label):
    def __init__(self, text, pos=(0, 0), size=(150, 50), color=Color.BLUE):
        super().__init__(text, pos, size)
        self._clicked = False
        self._hover = False
        self.color = color
        self.border = 2
        self.on_click = lambda sender, event: None
        self.set_color()

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, color):
        self._color = color
        self.set_color()

    @property
    def on_click(self):
        return self._on_click

    @on_click.setter
    def on_click(self, on_click):
        self._on_click = on_click

    def mouse_in(self, event):
        self._hover = True
        self.set_color()

    def mouse_out(self, event):
        self._hover = False
        self.set_color()

    def mouse_button_down(self, event):
        self._clicked = True
        self.set_color()

    def mouse_button_up(self, event):
        if self._clicked:
            self.on_click(self, event)
        self._clicked = False
        self.set_color()

    def set_color_default(self):
        self.back_color = self.color[6]
        self.border_color = self.color[7]

    def set_color_hover(self):
        self.back_color = self.color[4]
        self.border_color = self.color[0]

    def set_color_click(self):
            self.back_color = self.color[2]
            self.border_color = self.color[5]

    def set_color(self):
        if self._hover and self._clicked:
            self.back_color = self.color[2]
            self.border_color = self.color[5]
        elif self._hover:
            self.back_color = self.color[4]
            self.border_color = self.color[0]
        else:
            self.back_color = self.color[6]
            self.border_color = self.color[7]


class TextBox(Label):
    def __init__(self, text='', pos=(0, 0), size=(150, 50), color=Color.BLUE):
        super().__init__(text, pos, size)
        self.border = 2
        self.font = pg.font.match_font('lucidaconsole')
        self.color = color
        self.active = False
        self.on_return = lambda : None
        self.text_align = HAlign.LEFT, VAlign.CENTER
        self.cursor = '_'
        self.place_holder = ''
        self.padding = 20, 0
        self.prohibited = set()
        self.limit = None
        self._clicked = False

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, color):
        self._color = color

    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, active):
        self._active = active
        if active:
            self.back_color = self.color[6]
            self.border_color = self.color[0]
        else:
            self.back_color = self.color[7]
            self.border_color = self.color[6]

    @property
    def on_return(self):
        return self._on_return

    @on_return.setter
    def on_return(self, on_return):
        self._on_return = on_return

    @property
    def place_holder(self):
        return self._place_holder

    @place_holder.setter
    def place_holder(self, place_holder):
        self._place_holder = place_holder

    @property
    def cursor(self):
        return self._cursor

    @cursor.setter
    def cursor(self, cursor):
        self._cursor = cursor

    @property
    def padding(self):
        return self._padding

    @padding.setter
    def padding(self, padding):
        self._padding = padding
    
    @property
    def prohibited(self):
        return self._prohibited
    
    @prohibited.setter
    def prohibited(self, prohibited):
        self._prohibited = prohibited

    @property
    def limit(self):
        return self._limit

    @limit.setter
    def limit(self, limit):
        self._limit = limit

    @property
    def text_rect(self):
        text_rect = super().text_rect
        direction = mul(dot(self.text_align, self.padding), -1)
        text_rect.move_ip(*direction)
        return text_rect

    def mouse_button_down(self, event):
        self._clicked = True

    def mouse_button_down_outside(self, event):
        self.active = False
        self._clicked = False

    def mouse_button_up(self, event):
        self.active = self._clicked
        self._clicked = False

    def mouse_button_up_outside(self, event):
        self._clicked = False

    def key_down(self, event):
        if not self.active:
            return
        if event.key == pg.K_RETURN:
            self.on_return(self, event)
        elif event.key == pg.K_BACKSPACE:
            self.text = self.text[:-1]
        elif event.key in self.prohibited:
            pass
        elif self.limit and len(self.text) >= self.limit:
            pass
        else:
            self.text += event.unicode

    def draw(self, screen):
        super().draw(screen)
        if self.active and len(self.text) < self.limit:
            cursor_surface = get_text_surface(self.cursor, self.font, self.font_size, self.fore_color)
            cursor_rect = cursor_surface.get_rect()
            cursor_rect.topleft = self.text_rect.topright
            screen.blit(cursor_surface, cursor_rect)
        elif not self.active and not self.text:
            place_holder_surface = get_text_surface(self.place_holder, self.font, self.font_size, (80, 80, 80))
            place_holder_rect = place_holder_surface.get_rect()
            place_holder_rect.topleft = self.text_rect.topleft
            screen.blit(place_holder_surface, place_holder_rect)


# Panel


class Screen(GameObject):
    def __init__(self, app):
        super().__init__((0, 0), app.SIZE)
        self.done = False
        self.app = app

    @property
    def socket(self):
        return self.app.socket

    def run(self):
        while not self.done:
            self.listen()
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    exit(0)
                if event.type == pg.USEREVENT:
                    self.handle_message(event.connection, event.message)
                self.handle_event(event)
            event = pg.event.Event(TICK, {})
            pg.event.post(event)
            self.update()

    def update(self, rect=None):
        self.draw(self.app.screen)
        self.app.update(rect)

    def handle_message(self, connection, message):
        print(f'Incoming message: {message}')

    def listen(self):
        if self.socket.is_server:
            self.socket.get_connection()
        for connection, message in self.socket.receive_messages():
            event = pg.event.Event(pg.USEREVENT, {'connection': connection, 'message': message})
            pg.event.post(event)


class Square(GameObject):
    def __init__(self, index, map, image=None, location=None):
        super().__init__()
        self.index = index
        self.role = Map.get_role(index)
        self.map = map
        self.location = location
        self.col, self.row = 0, 0
        self.size = 100, 100
        self.image = image
        self.on_key_down = None

    @property
    def on_key_down(self):
        return self._on_key_down

    @on_key_down.setter
    def on_key_down(self, on_key_down):
        self._on_key_down = on_key_down

    def key_down(self, event):
        if self.on_key_down:
            self.on_key_down(self, event)

    def movable(self, d_index):
        dx = [-1, 1, 0, 0][d_index]
        dy = [0, 0, -1, 1][d_index]
        new_loc = self.col + dx, self.row + dy
        if new_loc not in self.map.free_cells(self.index):
            return False,
        new_portals = self.map.objects[Map.PORTAL:Map.PORTAL + 2]
        for i in range(Map.PORTAL, Map.PORTAL + 2):
            if new_loc == self.map.objects[i]:
                new_loc = self.map.objects[i ^ 1]
                new_portals = self.map.get_new_portals()
                break
        for i in range(2):
            if new_portals[i] == new_loc:
                new_portals[i] = self.col, self.row
        return True, new_loc, new_portals

    def draw(self, screen):
        self.col, self.row = self.location if self.location else self.map.objects[self.index]
        self.x = self.col * self.w
        self.y = self.row * self.h
        super().draw(screen)
        if self.image:
            self.x = self.col * self.w
            self.y = self.row * self.h
            screen.blit(self.image, self.true_rect)
