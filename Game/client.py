import socket
import select
from connection import Connection


class Client(Connection):
    def __init__(self, server_address=None):
        super().__init__()
        self.is_server = False
        if not server_address:
            server_address = socket.gethostbyname(socket.gethostname()), 9999
        self.server_address = server_address
        self.client_address = None
        self.client_socket = None

    def connect(self):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.client_socket.connect(self.server_address)
            return True
        except ConnectionRefusedError as e:
            error_message = 'Server is currently unavailable.'
            self.crashed(error_message, e)
            return error_message
        except TimeoutError as e:
            error_message = 'No response from the server.'
            self.crashed(error_message, e)
            return error_message
        except Exception as e:
            error_message = 'Cannot connect to the server.'
            self.crashed(error_message, e)
            return error_message

    def send_message(self, message):
        try:
            self.send(self.client_socket, message)
        except socket.error as e:
            self.crashed(f'Could not send a message to the server.', e)
        except Exception as e:
            self.crashed('Something went wrong while sending a message.', e)

    def receive_messages(self):
        if not self.client_socket:
            return
        read_sockets, _, exception_sockets = select.select([self.client_socket], [], [self.client_socket], 0)
        if not read_sockets:
            return
        try:
            for message in self.receive(self.client_socket):
                yield self.client_socket, message
        except socket.error as e:
            error_message = f'Could not get messages from the server.'
            self.crashed(error_message, e)
            yield self.client_socket, error_message
        except Exception as e:
            error_message = 'Something went wrong while receiving messages.'
            self.crashed(error_message, e)
            yield self.client_socket, error_message

    def close(self):
        self.client_socket.close()
        self.client_socket = None
