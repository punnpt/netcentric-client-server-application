import pygame as pg


def add(p, q):
    return p[0] + q[0], p[1] + q[1]


def minus(p, q):
    return p[0] - q[0], p[1] - q[1]


def dot(p, q):
    return p[0] * q[0], p[1] * q[1]


def mul(p, r):
    return p[0] * r, p[1] * r


def get_text_surface(text, font, size, color):
    return pg.font.Font(font, size).render(text, True, color)


class Color:
    BLUE = ((44, 73, 255), (54, 80, 239), (65, 87, 233), (75, 93, 207), (86, 100, 191), (96, 107, 175), (107, 114, 159), (117, 121, 143))
    LIGHT_BLUE = ((129, 248, 249), (129, 233, 234), (129, 218, 219), (128, 203, 203), (128, 188, 188), (128, 173, 173), (128, 158, 158), (128, 143, 143))


class HAlign:
    LEFT = -1
    CENTER = 0
    RIGHT = 1


class VAlign:
    TOP = -1
    CENTER = 0
    BOTTOM = 1


class GameObject:
    def __init__(self, pos=(0, 0), size=(50, 50)):
        self.rect = pg.Rect(pos, size)
        self.pos = pos
        self.size = size

        self.enabled = True
        self.fore_color = None
        self.back_color = None
        self.border_color = None
        self.border = None

        # parent and children obj
        self.parent = None
        self.children = []

    @property
    def rect(self):
        return self._rect

    @rect.setter
    def rect(self, rect):
        self._rect = rect

    @property
    def x(self):
        return self.rect.x

    @x.setter
    def x(self, x):
        self.rect.x = x

    @property
    def y(self):
        return self.rect.y

    @y.setter
    def y(self, y):
        self.rect.y = y

    @property
    def pos(self):
        return self.rect.topleft

    @pos.setter
    def pos(self, pos):
        self.rect.topleft = pos

    @property
    def center(self):
        return self.rect.center

    @center.setter
    def center(self, center):
        self.rect.center = center

    @property
    def w(self):
        return self.rect.w

    @w.setter
    def w(self, w):
        self.rect.w = w

    @property
    def h(self):
        return self.rect.h

    @h.setter
    def h(self, h):
        self.rect.h = h

    @property
    def size(self):
        return self.rect.size

    @size.setter
    def size(self, size):
        self.rect.size = size

    @property
    def true_rect(self):
        if not self.parent:
            return self.rect
        return self.rect.move(*self.parent.true_pos)

    @property
    def true_pos(self):
        return self.true_rect.topleft

    ###########################################

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def fore_color(self):
        return self._fore_color

    @fore_color.setter
    def fore_color(self, fore_color):
        self._fore_color = fore_color

    @property
    def back_color(self):
        return self._back_color

    @back_color.setter
    def back_color(self, back_color):
        self._back_color = back_color

    @property
    def border_color(self):
        return self._border_color

    @border_color.setter
    def border_color(self, border_color):
        self._border_color = border_color

    @property
    def border(self):
        return self._border

    @border.setter
    def border(self, border):
        self._border = border

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, parent):
        self._parent = parent

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, children):
        self._children = children

    # others

    def add(self, obj):
        if obj in self.children:
            return
        self.children.append(obj)
        obj.parent = self

    def remove(self, obj):
        if obj not in self.children:
            return
        self.children.remove(obj)
        obj.parent = None

    # events

    def mouse_in(self, event):
        pass

    def mouse_out(self, event):
        pass

    def mouse_button_down(self, event):
        pass

    def mouse_button_down_outside(self, event):
        pass

    def mouse_button_up(self, event):
        pass

    def mouse_button_up_outside(self, event):
        pass

    def key_down(self, event):
        pass

    # main function

    def handle_event(self, event):
        if not self.enabled:
            return
        if event.type == pg.MOUSEMOTION:
            if self.true_rect.collidepoint(event.pos):
                self.mouse_in(event)
            else:
                self.mouse_out(event)
        elif event.type == pg.MOUSEBUTTONDOWN:
            if self.true_rect.collidepoint(event.pos):
                self.mouse_button_down(event)
            else:
                self.mouse_button_down_outside(event)
        elif event.type == pg.MOUSEBUTTONUP:
            if self.true_rect.collidepoint(event.pos):
                self.mouse_button_up(event)
            else:
                self.mouse_button_up_outside(event)
        elif event.type == pg.KEYDOWN:
            self.key_down(event)
        for child in self.children:
            child.handle_event(event)

    def draw(self, screen):
        if self.back_color:
            pg.draw.rect(screen, self.back_color, self.true_rect)
        if self.border and self.border_color:
            pg.draw.rect(screen, self.border_color, self.true_rect, self.border)
        for child in self.children:
            child.draw(screen)


class User:
    def __init__(self, connection, address, name):
        self.connection = connection
        self.address = address
        self.name = name
        self.target = None  # connection of targeted user
        self.sitting = False
